/*

    Copyright (c) 2019 Xavier Blanchette-Noël

    L'autorisation est accordée, gracieusement, à toute personne acquérant une
    copie de cette bibliothèque et des fichiers de documentation associés
    (la "Bibliothèque"), de commercialiser la Bibliothèque sans restriction,
    notamment les droits d'utiliser, de copier, de modifier, de fusionner, de
    publier, de distribuer, de sous-licencier et / ou de vendre des copies de
    la Bibliothèque, ainsi que d'autoriser les personnes auxquelles la
    Bibliothèque est fournie à le faire, sous réserve des conditions suivantes:

    La déclaration de copyright ci-dessus et la présente autorisation doivent
    être incluses dans toutes copies ou parties substantielles de la
    Bibliothèque.

    LA BIBLIOTHÈQUE EST FOURNIE "TELLE QUELLE", SANS GARANTIE D'AUCUNE SORTE,
    EXPLICITE OU IMPLICITE, NOTAMMENT SANS GARANTIE DE QUALITÉ MARCHANDE,
    D’ADÉQUATION À UN USAGE PARTICULIER ET D'ABSENCE DE CONTREFAÇON. EN AUCUN
    CAS, LES AUTEURS OU TITULAIRES DU DROIT D'AUTEUR NE SERONT RESPONSABLES DE
    TOUT DOMMAGE, RÉCLAMATION OU AUTRE RESPONSABILITÉ, QUE CE SOIT DANS LE
    CADRE D'UN CONTRAT, D'UN DÉLIT OU AUTRE, EN PROVENANCE DE, CONSÉCUTIF À OU
    EN RELATION AVEC LA BIBLIOTHÈQUE OU SON UTILISATION, OU AVEC D'AUTRES
    ÉLÉMENTS DE LA BIBLIOTHÈQUE.

*/

/*
    \file liste_tableau.c
    Fichier contenant les routines pour la liste tableau.
*/ 

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "liste_tableau.h"

#define TAILLE_TAMPON_FICHIER 4096

struct liste_tableau {
    int taille_liste;
    int capacite_liste;
    int* debut_liste;
    bool a_erreur;
    char* message_erreur;
};

/*
    \brief Créer une liste tableau d'une longueur donnée par l'utilisateur.
    \param int longeur_tableau La longeur du tableau 
    \return Une liste tableau allouée en mémoire.
*/
liste_tableau* creer_liste_tableau(int longueur_tableau) 
{
    if (longueur_tableau == 0) {longueur_tableau = 1;}
    liste_tableau* liste_resultante;
    liste_resultante = calloc(1, sizeof(struct liste_tableau));
    if (liste_resultante) {
        liste_resultante->taille_liste = 0;
        liste_resultante->capacite_liste = longueur_tableau;
        liste_resultante->debut_liste = calloc(longueur_tableau, sizeof(int));
        liste_resultante->a_erreur = NULL;
        liste_resultante->message_erreur = calloc(ERREUR_TAILLE, sizeof(char));
    }

    return liste_resultante;
}

/*
    \brief Libère l'espace mémoire utilisée par la liste tableau.
    \param liste_tableau* liste La liste talbeau à détruire.
*/
void detruire_liste_tableau(liste_tableau* liste)
{
    free(liste->message_erreur);
    free(liste->debut_liste);
    free(liste);
}

/*
    \brief Fonction qui retourne la taille de la liste tableau.
    \param liste_tableau* liste La liste à savoir sa longueur.
    \return int La longeur du tableau.
*/
int taille_liste_tableau(liste_tableau* liste)
{
    retirer_erreur_liste_tableau(liste);
    return liste->taille_liste;
}

/*
    \brief Fonction qui vérifie sur la longeur de la liste est égale à la capacité de la liste.
    Si oui, la fonction double la capacité de la liste.
    \param liste_tableau* liste La liste à doubler.
*/
void verifier_capacite(liste_tableau* liste)
{
    retirer_erreur_liste_tableau(liste);
    if (liste->taille_liste == liste->capacite_liste) {
        liste->debut_liste = (int*) realloc(liste->debut_liste, (sizeof(int) * liste->capacite_liste * 2));
        liste->capacite_liste = liste->capacite_liste * 2;
    }
}

/*
    \brief Fonction qui ajoute un élément à la fin de la liste tableau.
    \param liste_tableau* liste La liste où ajouter l'élément.
    \param int element L'élément à ajouter à la liste.
    \note Si la capacité de la liste tableau n'est pas assez grande pour ajouter
    l'élément, sa capacité sera doublée pour permettre l'ajout de l'élément.
*/
void ajouter_liste_tableau(liste_tableau* liste, int element)
{
    verifier_capacite(liste);

    liste->debut_liste[liste->taille_liste] = element;
    liste->taille_liste++;
}

/*
    \brief Fonction qui retourne l'élément à l'index indiqué.
    \param liste_tableau* liste La liste où l'élément se trouve.
    \param int index L'index précis poù la valeur se retrouve.
    \return int La valeur à l'index précisé.
*/
int element_liste_tableau(liste_tableau* liste, int index)
{
    int resultat = 0;
    retirer_erreur_liste_tableau(liste);
    if (liste->taille_liste > 0) {
        if (index >= 0 && index <= liste->taille_liste) {
            resultat = liste->debut_liste[index];
        } else {
            inscrire_erreur_liste_tableau(liste, "L'index est hors limite.");
            resultat = -1;
        }
    } else {
        inscrire_erreur_liste_tableau(liste, "La liste est vide.");
    }

    return resultat;
}

/*
    \brief Fonction qui remplace un élément de la liste tableau par un autre élément à un index précis.
    \param liste_tableau* liste La liste tableau où on doit remplacer l'élément.
    \param int index L'index où la fonction remplacera l'élément.
    \param int element L'élément à mettre dans la liste tableau à place de l'ancien.
*/
void remplacer_liste_tableau(liste_tableau* liste, int index, int element)
{
    retirer_erreur_liste_tableau(liste);
    if (liste->taille_liste > 0) {
        if (index >= 0 && index <= liste->taille_liste) {
            liste->debut_liste[index] = element;
        } else {
            inscrire_erreur_liste_tableau(liste, "L'index est hors limite.");
        }
    } else {
        inscrire_erreur_liste_tableau(liste, "La liste est vide.");
    }
}

/*
    \brief Fonction qui insère un élément dans la liste tableau à un index précisé.
    \param liste_tableau* liste La liste où insérer l'élément.
    \param int index L'index où insérer l'élément.
    \param int element L'élément à ajouter dans la liste.
*/
void inserer_liste_tableau(liste_tableau* liste, int index, int element)
{
    int i;
    verifier_capacite(liste);
    retirer_erreur_liste_tableau(liste);
    if (index >= 0 && index <= liste->taille_liste) {
        for (i = liste->taille_liste; i >= index; i--) {
            liste->debut_liste[i+1] = liste->debut_liste[i];
        }
    liste->debut_liste[index] = element;
    liste->taille_liste++;
    } else {
        inscrire_erreur_liste_tableau(liste, "L'index est hors limite.");
    }
}

/*
    \brief Fonction qui retire un élément de la liste tableau à un index donné.
    \param liste_tableau* liste La liste où retirer l'élément.
    \param int index La position où retirer l'élément.
*/
void retirer_liste_tableau(liste_tableau* liste, int index)
{
    int i;
    retirer_erreur_liste_tableau(liste);
    if (liste->taille_liste > 0) {
        if (index >= 0 && index <= liste->taille_liste) {
            for (i = index; i <= liste->taille_liste; i++) {
                liste->debut_liste[i-1] = liste->debut_liste[i];
            }
        liste->taille_liste--;
        } else {
            inscrire_erreur_liste_tableau(liste, "L'index est hors limite.");
        }
    } else {
        inscrire_erreur_liste_tableau(liste, "La liste est vide.");
    }
}

/*
    \brief Fonction qui sauvegarde les données contenues dans la liste tableau dans un fichier.
    \param liste_tableau* liste La liste avec les données à sauvegarder.
    \param char* nom_fichier Le nom du fichier où les données seront sauvegardées.
*/
void sauvegarder_liste_tableau(liste_tableau* liste, char* nom_fichier) {

    FILE* fichier = fopen(nom_fichier, "wb");
    retirer_erreur_liste_tableau(liste);
    if (fichier) {
        fwrite(liste->debut_liste, sizeof(int), liste->taille_liste, fichier);
        fclose(fichier);
    } else {
        inscrire_erreur_liste_tableau(liste, "Incapable d'ouvrir le fichier.");
    }
}

/*
    \brief Retourne une nouvelle liste tableau créée à l'aide de données contenues dans le fichier "nom_fichier".
    \param char* nom_fichier Le nom du fichier où se trouve les données à charger dans la liste.
    \note Le fichier doit être créé à l'aide la routine "sauvegarder_liste_tableau".
    \return liste_tableau* Une liste tableau avec toutes les données insérées.
*/
liste_tableau* charger_liste_tableau(char* nom_fichier) {
    
    liste_tableau* liste;
    FILE* fichier;
    int capacite_tableau = 0; 

    fichier = fopen(nom_fichier, "rb");
    if (fichier) {
        fseek(fichier, 0, SEEK_END);
        capacite_tableau = ftell(fichier) / sizeof(int);
        liste = creer_liste_tableau(capacite_tableau);
        fseek(fichier, 0, SEEK_SET);
        fread(liste->debut_liste, sizeof(int), capacite_tableau, fichier);
        liste->taille_liste = capacite_tableau;
        fclose(fichier);
    } else {
        liste = creer_liste_tableau(0);
        inscrire_erreur_liste_tableau(liste, "Incapable d'ouvrir le fichier.");
    }

    return liste;
}

/*
    \brief Fonction qui retourne un bool si la fonction à rencontrer 
    une erreur lors du denrier appel de la fonction de la librairie.
    \param liste_tableau* liste La liste où le bool est contenue.
    \return Vrai si la liste à recontrer une erreur lors du dernier appel de fonction.
*/
bool a_erreur_liste(liste_tableau* liste)
{
    return liste->a_erreur = true;
}

/*
    \brief Fonction qui retourne un message d'erreur si a_erreur_liste est vrai.
    \param liste_tableau* liste La liste tableau où l'erreur est contenue.
    \return char* Le message d'erreur.
*/
char* erreur_liste_tableau(liste_tableau* liste)
{
    if (!liste->a_erreur) {
        strncpy(liste->message_erreur, "", ERREUR_TAILLE);
    }

    return liste->message_erreur;
}

/*
    \brief Fonction qui insert un message d'erreur dans la liste tableau.
    \param liste_tableau* liste La liste où le message d'erreur sera sauvegarder.
    \param char* erreur Le message d'erreur à mettre dans la liste tableau.
*/
void inscrire_erreur_liste_tableau(liste_tableau* liste, const char* erreur)
{
    liste->a_erreur = true;
    strncpy(liste->message_erreur, erreur, ERREUR_TAILLE);
}

/*
    \brief Fonction qui met a_erreur_liste_tableau à faux.
    \param liste_tableau* liste La liste où retirer l'erreur.
*/
void retirer_erreur_liste_tableau(liste_tableau* liste)
{
    liste->a_erreur = false;
}