#include <stdbool.h>

#define ERREUR_TAILLE 255

/*
    \file file.h
    Fichier d'entête contenant la structure de donnée "file".
    Une file est une structure de donnée où le premier élément ajouté sur la pile
    est le premier élément à sortir de la pile.
*/ 

/*
    Structure implémentée de file
*/
typedef struct liste_tableau liste_tableau;

/*
    \brief Créer une liste tableau d'une longueur donnée par l'utilisateur.
    \param int longeur_tableau La longeur du tableau 
    \return Une liste tableau allouée en mémoire.
*/
liste_tableau* creer_liste_tableau(int longeur_tableau);

/*
    \brief Libère l'espace mémoire utilisée par la liste tableau.
    \param liste_tableau* liste La liste talbeau à détruire.
*/
void detruire_liste_tableau(liste_tableau* liste);

/*
    \brief Fonction qui retourne la taille de la liste tableau.
    \param liste_tableau* liste La liste à savoir sa longueur.
    \return int La longeur du tableau.
*/
int taille_liste_tableau(liste_tableau* liste);

/*
    \brief Fonction qui ajoute un élément à la fin de la liste tableau.
    \param liste_tableau* liste La liste où ajouter l'élément.
    \param int element L'élément à ajouter à la liste.
    \note Si la capacité de la liste tableau n'est pas assez grande pour ajouter
    l'élément, sa capacité sera doublée pour permettre l'ajout de l'élément.
*/
void ajouter_liste_tableau(liste_tableau* liste, int element);

/*
    \brief Fonction qui retourne l'élément à l'index indiqué.
    \param liste_tableau* liste La liste où l'élément se trouve.
    \param int index L'index précis poù la valeur se retrouve.
    \return int La valeur à l'index précisé.
*/
int element_liste_tableau(liste_tableau* liste, int index);

/*
    \brief Fonction qui insère un élément dans la liste tableau à un index précisé.
    \param liste_tableau* liste La liste où insérer l'élément.
    \param int index L'index où insérer l'élément.
    \param int element L'élément à ajouter dans la liste.
*/
void inserer_liste_tableau(liste_tableau* liste, int index, int element);

/*
    \brief Fonction qui remplace un élément de la liste tableau par un autre élément à un index précis.
    \param liste_tableau* liste La liste tableau où on doit remplacer l'élément.
    \param int index L'index où la fonction remplacera l'élément.
    \param int element L'élément à mettre dans la liste tableau à place de l'ancien.
*/
void remplacer_liste_tableau(liste_tableau* liste, int index, int element);

/*
    \brief Fonction qui retire un élément de la liste tableau à un index donné.
    \param liste_tableau* liste La liste où retirer l'élément.
    \param int index La position où retirer l'élément.
*/
void retirer_liste_tableau(liste_tableau* liste, int index);

/*
    \brief Fonction qui sauvegarde les données contenues dans la liste tableau dans un fichier.
    \param liste_tableau* liste La liste avec les données à sauvegarder.
    \param char* nom_fichier Le nom du fichier où les données seront sauvegardées.
*/
void sauvegarder_liste_tableau(liste_tableau* liste, char* nom_fichier);

/*
    \brief Retourne une nouvelle liste tableau créée à l'aide de données contenues dans le fichier "nom_fichier".
    \param char* nom_fichier Le nom du fichier où se trouve les données à charger dans la liste.
    \note Le fichier doit être créé à l'aide la routine "sauvegarder_liste_tableau".
    \return liste_tableau* Une liste tableau avec toutes les données insérées.
*/
liste_tableau* charger_liste_tableau(char* nom_fichier);

/*
    \brief Fonction qui vérifie sur la longeur de la liste est égale à la capacité de la liste.
    Si oui, la fonction double la capacité de la liste.
    \param liste_tableau* liste La liste à doubler.
*/
void verifier_capacite(liste_tableau* liste);

/*
    \brief Fonction qui retourne un bool si la fonction à rencontrer 
    une erreur lors du denrier appel de la fonction de la librairie.
    \param liste_tableau* liste La liste où le bool est contenue.
    \return Vrai si la liste à recontrer une erreur lors du dernier appel de fonction.
*/
bool a_erreur_liste(liste_tableau* liste);

/*
    \brief Fonction qui retourne un message d'erreur si a_erreur_liste est vrai.
    \param liste_tableau* liste La liste tableau où l'erreur est contenue.
    \return char* Le message d'erreur.
*/
char* erreur_liste_tableau(liste_tableau* liste);

/*
    \brief Fonction qui insert un message d'erreur dans la liste tableau.
    \param liste_tableau* liste La liste où le message d'erreur sera sauvegarder.
    \param char* erreur Le message d'erreur à mettre dans la liste tableau.
*/
void inscrire_erreur_liste_tableau(liste_tableau* liste, const char* erreur);

/*
    \brief Fonction qui met a_erreur_liste_tableau à faux.
    \param liste_tableau* liste La liste où retirer l'erreur.
*/
void retirer_erreur_liste_tableau(liste_tableau* liste);

void printListe(liste_tableau* a_liste);