/*

    Copyright (c) 2019 Xavier Blanchette-Noël

    L'autorisation est accordée, gracieusement, à toute personne acquérant une
    copie de cette bibliothèque et des fichiers de documentation associés
    (la "Bibliothèque"), de commercialiser la Bibliothèque sans restriction,
    notamment les droits d'utiliser, de copier, de modifier, de fusionner, de
    publier, de distribuer, de sous-licencier et / ou de vendre des copies de
    la Bibliothèque, ainsi que d'autoriser les personnes auxquelles la
    Bibliothèque est fournie à le faire, sous réserve des conditions suivantes:

    La déclaration de copyright ci-dessus et la présente autorisation doivent
    être incluses dans toutes copies ou parties substantielles de la
    Bibliothèque.

    LA BIBLIOTHÈQUE EST FOURNIE "TELLE QUELLE", SANS GARANTIE D'AUCUNE SORTE,
    EXPLICITE OU IMPLICITE, NOTAMMENT SANS GARANTIE DE QUALITÉ MARCHANDE,
    D’ADÉQUATION À UN USAGE PARTICULIER ET D'ABSENCE DE CONTREFAÇON. EN AUCUN
    CAS, LES AUTEURS OU TITULAIRES DU DROIT D'AUTEUR NE SERONT RESPONSABLES DE
    TOUT DOMMAGE, RÉCLAMATION OU AUTRE RESPONSABILITÉ, QUE CE SOIT DANS LE
    CADRE D'UN CONTRAT, D'UN DÉLIT OU AUTRE, EN PROVENANCE DE, CONSÉCUTIF À OU
    EN RELATION AVEC LA BIBLIOTHÈQUE OU SON UTILISATION, OU AVEC D'AUTRES
    ÉLÉMENTS DE LA BIBLIOTHÈQUE.

*/

/*
    \file main.c
    Effectue les tests pour la liste tableau.
*/

#include <stdio.h>
#include <stdlib.h>

#include "liste_tableau.h"

/*
    \brief Tests des listes tableau.
*/
int main() 
{
    liste_tableau* liste = creer_liste_tableau(5);
    liste_tableau* liste2;
    
    if (liste) {
        ajouter_liste_tableau(liste, 0);    
        ajouter_liste_tableau(liste, 1);    
        ajouter_liste_tableau(liste, 2);    
        ajouter_liste_tableau(liste, 3);    
        ajouter_liste_tableau(liste, 4);
        
        for (int i = 0; i < 5; i++) {
            printf("%d ", element_liste_tableau(liste, i));
        }

        printf("S'il y a eu erreur, la voici : %s\n", erreur_liste_tableau(liste));

        printf("La taille de la liste est de : %d\n", taille_liste_tableau(liste));

        printf("Le 3ème élément du tableau devrait être 2 et voici la vraie valeur : %d\n", element_liste_tableau(liste, 2));

        remplacer_liste_tableau(liste, 2, 15);
        printf("S'il y a eu erreur, la voici : %s\n", erreur_liste_tableau(liste));

        for (int i = 0; i < 5; i++) {
            printf("%d ", element_liste_tableau(liste, i));
        }

        printf("Le 3ème élément devrait maintenant être le chiffre 15 et voici la vraie valeur : %d\n", element_liste_tableau(liste, 2));

        inserer_liste_tableau(liste, 2, 1000);
        printf("S'il y a eu erreur, la voici : %s\n", erreur_liste_tableau(liste));

        for (int i = 0; i < 6; i++) {
            printf("%d ", element_liste_tableau(liste, i));
        }

        printf("Le 3ème élément devrait maintenant être le chiffre 1000 et voici la vraie valeur : %d\n", element_liste_tableau(liste, 2));

        retirer_liste_tableau(liste, 2);
        printf("S'il y a eu erreur, la voici : %s\n", erreur_liste_tableau(liste));
        
        for (int i = 0; i < 5; i++) {
            printf("%d ", element_liste_tableau(liste, i));
        }

        printf("Le 3ème élément devrait maintenant être redevenu le chiffre 15 et voici la vraie valeur : %d\n", element_liste_tableau(liste, 2));

        sauvegarder_liste_tableau(liste, "test.bin");
        printf("S'il y a eu erreur, la voici : %s\n", erreur_liste_tableau(liste));

        detruire_liste_tableau(liste);

        printf("La liste 1 est maintenant détruite.");

        liste2 = charger_liste_tableau("test.bin");

        if (liste2) {
            printf("S'il y a eu erreur, la voici : %s\n", erreur_liste_tableau(liste));
            
            //printf("Le 3ème élément du tableau devrait être le chiffre 15 et voici la vraie valeur : %d\n", element_liste_tableau(liste2, 2));

        } else {
            printf("Incapable de charger la liste à partir du fichier 'test.bin'.");
        }

        detruire_liste_tableau(liste2);

        printf("La liste2 a été détruite.\n");

    } else {
        printf("La liste n'a pas pu être créée.\n");
    }

    return 0;
}